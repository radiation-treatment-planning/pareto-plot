﻿using System.Collections.Generic;
using System.Windows.Controls;
using MathNet.Spatial.Euclidean;
using OxyPlot;
using Pareto;
using ParetoPlotUserControl;
using Prism.Commands;
using Prism.Mvvm;

namespace ExampleApp.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        public MainWindowViewModel()
        {
            CustomPlotController = new PlotController();
            CustomPlotTitle = "Custom plot title";
            CustomXAxisTitle = "x-axis title";
        }

        private string _title = "Pareto Plot example App";

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private Pareto.Pareto _customParetoData;

        public Pareto.Pareto CustomParetoData
        {
            get { return _customParetoData; }
            set { SetProperty(ref _customParetoData, value); }
        }

        private PlotController _customPlotController;

        public PlotController CustomPlotController
        {
            get => _customPlotController;
            set => SetProperty(ref _customPlotController, value);
        }

        private bool _plotParetoLines = true;
        public bool PlotParetoLines
        {
            get { return _plotParetoLines; }
            set { SetProperty(ref _plotParetoLines, value); }
        }

        private DelegateCommand _createNewParetoCommand;

        public DelegateCommand CreateNewParetoCommand =>
            _createNewParetoCommand ?? (_createNewParetoCommand = new DelegateCommand(ExecuteCreateNewParetoCommand));

        void ExecuteCreateNewParetoCommand()
        {
            CustomParetoData = CreateARandomPareto();
            CanExecuteFurtherCommands = true;
        }

        private DelegateCommand _createNewPlotControllerCommand;
        public DelegateCommand CreateNewPlotControllerCommand =>
            _createNewPlotControllerCommand ?? (_createNewPlotControllerCommand =
                new DelegateCommand(ExecuteCreateNewPlotControllerCommand)
                    .ObservesCanExecute(() => CanExecuteFurtherCommands));

        void ExecuteCreateNewPlotControllerCommand()
        {
            var plotController = new PlotController();
            plotController.UnbindMouseDown(OxyMouseButton.Left);
            plotController.BindMouseEnter(PlotCommands.HoverSnapTrack);
            CustomPlotController = plotController;
        }

        private bool _canExecuteFurtherCommands;
        public bool CanExecuteFurtherCommands
        {
            get { return _canExecuteFurtherCommands; }
            set { SetProperty(ref _canExecuteFurtherCommands, value); }
        }

        private string _customPlotTitle;
        public string CustomPlotTitle
        {
            get { return _customPlotTitle; }
            set { SetProperty(ref _customPlotTitle, value); }
        }

        private DelegateCommand _createNewPlotTitleCommand;
        public DelegateCommand CreateNewPlotTitleCommand =>
            _createNewPlotTitleCommand ?? (_createNewPlotTitleCommand = new DelegateCommand(ExecuteCreateNewPlotTitleCommand)
                .ObservesCanExecute(() => CanExecuteFurtherCommands));

        void ExecuteCreateNewPlotTitleCommand() => CustomPlotTitle = "New Plot Title";

        private string _customXAxisTitle;
        public string CustomXAxisTitle
        {
            get { return _customXAxisTitle; }
            set { SetProperty(ref _customXAxisTitle, value); }
        }
        private DelegateCommand _changeXAxisTitleCommand;
        public DelegateCommand ChangeXAxisTitleCommand =>
            _changeXAxisTitleCommand ?? (_changeXAxisTitleCommand = new DelegateCommand(ExecuteChangeXAxisTitleCommand)
                .ObservesCanExecute(() => CanExecuteFurtherCommands));

        void ExecuteChangeXAxisTitleCommand() => CustomXAxisTitle = "New x-axis title";

        private ZoomArea _customZoomArea;
        public ZoomArea CustomZoomArea
        {
            get { return _customZoomArea; }
            set { SetProperty(ref _customZoomArea, value); }
        }

        private DelegateCommand _changeZoomAreaCommand;
        public DelegateCommand ChangeZoomAreaCommand =>
            _changeZoomAreaCommand ?? (_changeZoomAreaCommand = new DelegateCommand(ExecuteChangeZoomAreaCommand)
                .ObservesCanExecute(() => CanExecuteFurtherCommands));

        void ExecuteChangeZoomAreaCommand() => CustomZoomArea = new ZoomArea(0.0, 0.3, 0.0, 0.4);

        private DelegateCommand _changeLineTrackerCommand;
        public DelegateCommand ChangeLineTrackerCommand =>
            _changeLineTrackerCommand ?? (_changeLineTrackerCommand
                = new DelegateCommand(ExecuteChangeLineTrackerCommand)
                .ObservesCanExecute(() => CanExecuteFurtherCommands));

        void ExecuteChangeLineTrackerCommand() => PlotParetoLines = !PlotParetoLines;

        private Pareto.Pareto CreateARandomPareto()
        {
            var paretoPoints = new[]
            {
                ParetoPoint.Factory.CreateDominatingParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                        { ("point", "1"), ("key2", "false") }), new Point2D(0.1, 0.8), OxyColors.DarkRed,
                    MarkerType.Diamond),
                ParetoPoint.Factory.CreateDominatingParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("point", "3") }),
                    new Point2D(0.2, 0.3), OxyColors.DarkRed, MarkerType.Diamond),
                ParetoPoint.Factory.CreateDominatingParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("point", "5") }),
                    new Point2D(0.5, 0.2), OxyColors.DarkRed, MarkerType.Diamond),
                ParetoPoint.Factory.CreateDominatingParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("point", "7") }),
                    new Point2D(0.8, 0.1), OxyColors.DarkRed, MarkerType.Diamond),

                ParetoPoint.Factory.CreateIllusionParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("x/y", "(0.1, 0.1)") }),
                    new Point2D(0.1, 0.1), OxyColors.Goldenrod, MarkerType.Square),

                ParetoPoint.Factory.CreateParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("point", "2") }),
                    new Point2D(0.2, 0.6), OxyColors.DarkBlue, MarkerType.Circle, "Pareto cluster 1"),
                ParetoPoint.Factory.CreateParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("point", "4") }),
                    new Point2D(0.4, 0.5), OxyColors.DarkBlue, MarkerType.Circle, "Pareto cluster 1"),
                ParetoPoint.Factory.CreateParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("point", "6") }),
                    new Point2D(0.7, 0.4), OxyColors.Green, MarkerType.Triangle, "Pareto cluster 2"),
                ParetoPoint.Factory.CreateParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("point", "10") }),
                    new Point2D(0.65, 0.45), OxyColors.Green, MarkerType.Triangle, "Pareto cluster 2"),

                ParetoPoint.Factory.CreateUniqueParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("point", "8") }),
                    new Point2D(0.5, 0.45), OxyColors.BlueViolet, MarkerType.Square, "Very unique point"),
                ParetoPoint.Factory.CreateUniqueParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>() { ("point", "9") }),
                    new Point2D(0.32, 0.71), OxyColors.IndianRed, MarkerType.Triangle, "Also unique point"),
            };

            var paretoLines = new ParetoLine[]
            {
                ParetoLine.Factory.CreateParetoFrontLine(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)> { ("line", "1") }),
                    new Point2D[]
                    {
                        new Point2D(0, 1), new Point2D(0.06, 0.7), new Point2D(0.1, 0.5), new Point2D(0.2, 0.3),
                        new Point2D(0.5, 0.15), new Point2D(0.8, 0.05), new Point2D(1, 0)
                    }, OxyColors.DarkRed, LineStyle.Dash),
                ParetoLine.Factory.CreateDiagonalLine(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)> { ("line", "1") }),
                    new Point2D[] { new Point2D(0, 0), new Point2D(1, 1) }, OxyColors.Goldenrod, LineStyle.DashDashDotDot),
            };

            return new Pareto.Pareto(paretoPoints, paretoLines);
        }
    }
}
