﻿using System.Windows;
using System.Windows.Controls;
using OxyPlot;
using Pareto;

namespace ParetoPlotUserControl
{
    /// <summary>
    /// Interaction logic for ParetoPlotView.xaml
    /// </summary>
    public partial class ParetoPlotView : UserControl
    {
        private readonly ParetoPlotUpdater _paretoPlotUpdater;

        public ParetoPlotView()
        {
            _paretoPlotUpdater = new ParetoPlotUpdater();
            InitializeComponent();
            InitializeAllDependencyProperties();
        }

        /*
         * Dependency Properties.
         */

        /// <summary>
        /// The pareto that shall be visualized.
        /// </summary>
        public Pareto.Pareto ParetoData
        {
            get => (Pareto.Pareto)GetValue(ParetoDataProperty);
            set { SetValue(ParetoDataProperty, value); }
        }

        public static readonly DependencyProperty ParetoDataProperty =
            DependencyProperty.Register("ParetoData", typeof(Pareto.Pareto),
                typeof(ParetoPlotView),
                new PropertyMetadata(null,
                    ParetoPropertyChangedCallback));

        internal PlotModel OxyPlotModel
        {
            get { return (PlotModel)GetValue(OxyPlotModelProperty); }
            private set { SetValue(OxyPlotModelProperty, value); }
        }

        public static readonly DependencyProperty OxyPlotModelProperty =
            DependencyProperty.Register("OxyPlotModel", typeof(PlotModel),
                typeof(ParetoPlotView), new PropertyMetadata(null));

        public PlotController OxyPlotController
        {
            get { return (PlotController)GetValue(OxyPlotControllerProperty); }
            set { SetValue(OxyPlotControllerProperty, value); }
        }

        public static readonly DependencyProperty OxyPlotControllerProperty =
            DependencyProperty.Register("OxyPlotController", typeof(PlotController),
                typeof(ParetoPlotView), new PropertyMetadata(null));


        public string PlotTitle
        {
            get { return (string)GetValue(PlotTitleProperty); }
            set { SetValue(PlotTitleProperty, value); }
        }

        public static readonly DependencyProperty PlotTitleProperty =
            DependencyProperty.Register("PlotTitle", typeof(string), typeof(ParetoPlotView),
                new PropertyMetadata(null, PlotTitlePropertyChangedCallback));

        public string XAxisTitle
        {
            get { return (string)GetValue(XAxisTitleProperty); }
            set { SetValue(XAxisTitleProperty, value); }
        }

        public static readonly DependencyProperty XAxisTitleProperty =
            DependencyProperty.Register("XAxisTitle", typeof(string), typeof(ParetoPlotView),
                new PropertyMetadata(null, XAxisTitlePropertyChangedCallback)
                );

        public string YAxisTitle
        {
            get { return (string)GetValue(YAxisTitleProperty); }
            set { SetValue(YAxisTitleProperty, value); }
        }

        public static readonly DependencyProperty YAxisTitleProperty =
            DependencyProperty.Register("YAxisTitle", typeof(string), typeof(ParetoPlotView),
                new PropertyMetadata(null, YAxisTitlePropertyChangedCallback));


        public bool IsLegendVisible
        {
            get { return (bool)GetValue(IsLegendVisibleProperty); }
            set { SetValue(IsLegendVisibleProperty, value); }
        }

        public static readonly DependencyProperty IsLegendVisibleProperty =
            DependencyProperty.Register("IsLegendVisible", typeof(bool), typeof(ParetoPlotView),
                new PropertyMetadata(true, IsLegendVisiblePropertyChangedCallback));


        public ZoomArea ZoomArea
        {
            get { return (ZoomArea)GetValue(ZoomAreaProperty); }
            set { SetValue(ZoomAreaProperty, value); }
        }

        public static readonly DependencyProperty ZoomAreaProperty =
            DependencyProperty.Register("ZoomArea", typeof(ZoomArea), typeof(ParetoPlotView),
                new PropertyMetadata(null, ZoomAreaPropertyChangedCallback));



        public bool PlotTagOfParetoLines
        {
            get { return (bool)GetValue(PlotTagOfParetoLinesProperty); }
            set { SetValue(PlotTagOfParetoLinesProperty, value); }
        }

        public static readonly DependencyProperty PlotTagOfParetoLinesProperty =
            DependencyProperty.Register("PlotTagOfParetoLines", typeof(bool), typeof(ParetoPlotView),
                new PropertyMetadata(true, PlotTagOfParetoLinesPropertyChangedCallback));


        /*
         * Callback Methods.
         */

        private static void ParetoPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (ParetoPlotView)d;
            control.ParetoData = e.NewValue as Pareto.Pareto;
            control.UpdateParetoPlot();
        }

        private static void PlotTitlePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (ParetoPlotView)d;
            control.PlotTitle = e.NewValue as string;
            control.UpdateParetoPlot();
        }

        private static void XAxisTitlePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (ParetoPlotView)d;
            control.XAxisTitle = e.NewValue as string;
            control.UpdateParetoPlot();
        }

        private static void YAxisTitlePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (ParetoPlotView)d;
            control.YAxisTitle = e.NewValue as string;
            control.UpdateParetoPlot();
        }
        private static void IsLegendVisiblePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (ParetoPlotView)d;
            control.IsLegendVisible = (bool)e.NewValue;
            control.UpdateParetoPlot();
        }

        private static void ZoomAreaPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (ParetoPlotView)d;
            control.ZoomArea = (ZoomArea)e.NewValue;
            control.UpdateParetoPlot();
        }
        private static void PlotTagOfParetoLinesPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (ParetoPlotView)d;
            control.PlotTagOfParetoLines = (bool)e.NewValue;
            control.UpdateParetoPlot();
        }


        /*
         * Helper methods.
         */
        private void UpdateParetoPlot()
        {
            OxyPlotModel = _paretoPlotUpdater.Update(OxyPlotModel, ParetoData, PlotTitle, XAxisTitle, YAxisTitle,
                IsLegendVisible, ZoomArea, PlotTagOfParetoLines);
        }
        private void InitializeAllDependencyProperties()
        {
            SetValue(OxyPlotModelProperty, new PlotModel());
            SetValue(ParetoDataProperty, new Pareto.Pareto(new ParetoPoint[] { }, new ParetoLine[] { }));
            SetValue(OxyPlotControllerProperty, new PlotController());
            SetValue(PlotTitleProperty, "Pareto Plot");
            SetValue(XAxisTitleProperty, "x-axis");
            SetValue(YAxisTitleProperty, "y-axis");
            SetValue(IsLegendVisibleProperty, true);
            SetValue(ZoomAreaProperty, null);
            SetValue(PlotTagOfParetoLinesProperty, true);
        }
    }
}
