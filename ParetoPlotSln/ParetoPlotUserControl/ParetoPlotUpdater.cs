﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Legends;
using OxyPlot.Series;
using Pareto;

namespace ParetoPlotUserControl
{
    public class ParetoPlotUpdater
    {
        public PlotModel Update(PlotModel plotModel,
            Pareto.Pareto pareto,
            string plotTitle,
            string xAxisTitle,
            string yAxisTitle,
            bool isLegendVisible,
            ZoomArea zoomArea,
            bool canTrackerInterpolateLines = true)
        {
            if (pareto is null) return plotModel;
            plotModel = new PlotModel { Title = plotTitle, IsLegendVisible = isLegendVisible };
            plotModel.Legends.Add(new Legend { LegendTitle = "Legend", LegendBackground = OxyColors.White });
            plotModel = AddAxes(plotModel, xAxisTitle, yAxisTitle, zoomArea, pareto);
            var paretoLineSeries = GetParetoLineSeries(pareto, canTrackerInterpolateLines);
            foreach (var lineSeries in paretoLineSeries) plotModel.Series.Add(lineSeries);
            var paretoPointSeries = GetParetoPointSeries(pareto);
            foreach (var series in paretoPointSeries) plotModel.Series.Add(series);
            plotModel.InvalidatePlot(true);
            return plotModel;
        }

        private PlotModel AddAxes(PlotModel plotModel, string xAxisTitle, string yAxisTitle, ZoomArea zoomArea, Pareto.Pareto pareto)
        {
            var axisTitleDistance = 15;
            if (zoomArea != null)
            {
                plotModel.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Bottom,
                    Title = xAxisTitle,
                    Minimum = zoomArea.MinX,
                    Maximum = zoomArea.MaxX,
                    AxisTitleDistance = axisTitleDistance
                });
                plotModel.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Bottom,
                    Title = xAxisTitle,
                    Minimum = zoomArea.MinX,
                    Maximum = zoomArea.MaxX,
                    AxisTitleDistance = axisTitleDistance
                });
            }
            else
            {
                var maxX = pareto.Points.Any() ? pareto.Points.Max(x => x.Point.X) : double.NaN;
                var maxY = pareto.Points.Any() ? pareto.Points.Max(x => x.Point.Y) : double.NaN;
                var minOfAxes = 0;
                var maxOfAxes = pareto.Points.Any() ? Math.Max(maxX, maxY) * 1.05 : double.NaN;
                plotModel.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Bottom,
                    Title = xAxisTitle,
                    Minimum = minOfAxes,
                    Maximum = maxOfAxes
                });
                plotModel.Axes.Add(new LinearAxis
                { Position = AxisPosition.Left, Title = yAxisTitle, Minimum = minOfAxes, Maximum = maxOfAxes });
            }
            return plotModel;
        }

        private PlotModel ClearPlotModel(PlotModel plotModel)
        {
            plotModel.Series.Clear();
            plotModel.Axes.Clear();
            return plotModel;
        }

        private IEnumerable<LineSeries> GetParetoLineSeries(Pareto.Pareto pareto, bool canTrackerInterpolatePoints = true)
        {
            var lineSeries = new List<LineSeries>();
            foreach (var line in pareto.Lines)
            {
                var series = new LineSeries()
                { LineStyle = line.LineStyle, Color = line.Color, Title = line.Description, CanTrackerInterpolatePoints = canTrackerInterpolatePoints };
                series.Points.AddRange(line.Points.Select(x => new DataPoint(x.X, x.Y)));
                lineSeries.Add(series);
            }

            return lineSeries;
        }

        private IEnumerable<ScatterSeries> GetParetoPointSeries(Pareto.Pareto pareto)
        {
            var paretoPoints = pareto.Points;
            var paretoPointsDict = CreateParetoPointsDict(paretoPoints);
            return CreateScatterSeriesFromParetoPointsDict(paretoPointsDict);
        }

        private IEnumerable<ScatterSeries> CreateScatterSeriesFromParetoPointsDict(Dictionary<Type, List<ParetoPoint>> dict)
        {
            var scatterSeries = new List<ScatterSeries>();
            var uniqueParetoPointType =
                ParetoPoint.Factory.CreateUniqueParetoPoint("", Point2D.Origin, OxyColors.Black, MarkerType.Circle, "Tmp description").GetType();

            foreach (var key in dict.Keys)
            {
                if (key == uniqueParetoPointType)
                    scatterSeries.AddRange(CreateScatterSeriesForUniqueParetoPoints(dict[key]));
                else
                    scatterSeries.AddRange(CreateScatterSeriesForNonUniqueParetoPoints(dict[key]));
            }

            return scatterSeries;
        }

        private List<ScatterSeries> CreateScatterSeriesForNonUniqueParetoPoints(List<ParetoPoint> paretoPoints)
        {
            var groupedPoints = GroupPointsByDescription(paretoPoints);
            var paretoScatterSeries = new List<ScatterSeries>();
            foreach (var description in groupedPoints.Keys)
            {
                var points = groupedPoints[description];
                var firstPoint = points.First();
                var markerFillFolor = firstPoint.Color;
                var markerType = firstPoint.MarkerType;
                var title = firstPoint.Description;
                var scatterSeries = new ScatterSeries { MarkerFill = markerFillFolor, MarkerType = markerType, Title = title, TrackerFormatString = "{Tag}" };
                foreach (var paretoPoint in points)
                {
                    var scatterPoint = new ScatterPoint(paretoPoint.Point.X, paretoPoint.Point.Y, tag: paretoPoint.Tag);
                    scatterSeries.Points.Add(scatterPoint);
                }
                paretoScatterSeries.Add(scatterSeries);
            }

            return paretoScatterSeries;
        }

        private Dictionary<string, List<ParetoPoint>> GroupPointsByDescription(List<ParetoPoint> paretoPoints)
        {
            var dict = new Dictionary<string, List<ParetoPoint>>();
            foreach (var point in paretoPoints)
            {
                if (!dict.ContainsKey(point.Description))
                    dict.Add(point.Description, new List<ParetoPoint>() { point });
                else
                    dict[point.Description].Add(point);
            }

            return dict;
        }

        private List<ScatterSeries> CreateScatterSeriesForUniqueParetoPoints(List<ParetoPoint> paretoPoints)
        {
            var scatterSeries = new List<ScatterSeries>();
            foreach (var paretoPoint in paretoPoints)
            {
                var title = paretoPoint.Description;
                var markerFillColor = paretoPoint.Color;
                var markerType = paretoPoint.MarkerType;
                var series = new ScatterSeries
                {
                    Title = title,
                    MarkerFill = markerFillColor,
                    MarkerType = markerType,
                    TrackerFormatString = "{Tag}"
                };
                var scatterPoint = new ScatterPoint(paretoPoint.Point.X, paretoPoint.Point.Y, tag: paretoPoint.Tag);
                series.Points.Add(scatterPoint);
                scatterSeries.Add(series);
            }

            return scatterSeries;
        }

        private Dictionary<Type, List<ParetoPoint>> CreateParetoPointsDict(IEnumerable<ParetoPoint> paretoPoints)
        {
            var dict = new Dictionary<Type, List<ParetoPoint>>();
            foreach (var paretoPoint in paretoPoints)
            {
                var type = paretoPoint.GetType();
                if (dict.ContainsKey(type)) dict[type].Add(paretoPoint);
                else dict.Add(type, new List<ParetoPoint>() { paretoPoint });
            }

            return dict;
        }
    }
}
