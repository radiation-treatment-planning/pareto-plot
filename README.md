A custom control for plotting 2D pareto points and lines. For an example how to use, see the ExampleApp project.

# How to utilize
1. Install the package via NuGet package manager or however you prefer: https://www.nuget.org/packages/ParetoPlotUserControl/.
2. In the Header of your view embed the assebly:
```
xmlns:paretoPlot="clr-namespace:ParetoPlotUserControl;assembly=ParetoPlotUserControl"
```
3. Embed the plot control in your view body:
```
<paretoPlot:ParetoPlotView>
```
4. Now you can add data with the **ParetoData** property:
```
<paretoPlot:ParetoPlotView ParetoData="{Binding CustomParetoData, Mode=TwoWay}">
```
Make sure that the `Mode` is set to `TwoWay`. Then the plot will automatically update, when you change the data.

5. There are a bunch of other properties to configure the behaviour of the plot. Check them out in the source code.
6. Color, MarkerType and LineStyle are defined directly in the `ParetoData`. Check out the dependency https://gitlab.com/radiation-treatment-planning/pareto. 
